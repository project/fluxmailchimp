<?php

/**
 * @file
 * Contains MailchimpAccount.
 */

namespace Drupal\fluxmailchimp\Plugin\Service;

use Drupal\fluxmailchimp\FluxMailchimpClient;
use Drupal\fluxmailchimp\Plugin\Service\MailchimpAccountInterface;
use Drupal\fluxservice\Plugin\Entity\Account;
use Guzzle\Http\Client;
use Guzzle\Http\Url;
use Guzzle\Service\Builder\ServiceBuilder;

/**
 * Account plugin implementation for Mailchimp.
 */
class MailchimpAccount extends Account implements MailchimpAccountInterface {

  /**
   * The service base url.
   *
   * @var string
   */
  protected $serviceUrl = 'https://login.mailchimp.com';

  /**
   * {@inheritdoc}
   */
  public static function getAccountForOAuthCallback($key, $plugin) {
    $store = fluxservice_tempstore("fluxservice.account.{$plugin}");
    return $store->getIfOwner($key);
  }

  /**
   * {@inheritdoc}
   */
  public function client() {
    $service = $this->getService();
    return FluxMailchimpClient::factory(array(
      'dc' => $this->getDcString(),
      'access_token' => $this->getAccessToken(),
    ));
  }

  /**
   * Defines the plugin.
   */
  public static function getInfo() {
    return array(
      'name' => 'fluxmailchimp',
      'label' => t('Mailchimp account'),
      'description' => t('Provides Mailchimp integration for fluxkraft.'),
      'service' => 'fluxmailchimp',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareAccount() {
    parent::prepareAccount();
    $redirect = $this->getRedirectUrl($this);

    $settings = $this->data;
    $settings->set('redirect', $redirect);

    // Temporarily save the account entity so we can refer to it later.
    $store = fluxservice_tempstore("fluxservice.account.{$this->bundle()}");
    $store->setIfNotExists($this->identifier(), $this);

    // application begins the authorization process
    // by redirecting the user to the authorize_uri.
    drupal_goto(url("https://login.mailchimp.com/{$this->getAuthorizeUrl()}", array('query' => array(
      'client_id' => $this->getService()->getConsumerKey(),
      'redirect_uri' => $redirect,
      'response_type' => 'code',
    ))));
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return array(
      'access_token' => '',
      'dc' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array &$form_state) {
    $form['access_token'] = array(
      '#type' => 'item',
      '#title' => t('Access token'),
      '#markup' => $this->getAccessToken(),
    );

    $form['dc'] = array(
      '#type' => 'item',
      '#title' => t('Datacenter string'),
      '#markup' => $this->getDcString(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processOAuthCallback() {
    // Make an out-of-band request to the access_token_uri using the code.
    $response = $this->requestAccessToken($_REQUEST['code'], $this->data->get('redirect'));

    // Request "dc" param using metadata url.
    $response += $this->requestExtendedAccessToken($response['access_token']);
    // Do some additional processing (e.g. loading some account information
    // like the accountname).
    $this->processAuthorizedAccount($response);

    // Remove the temporarily stored account entity from the tempstore.
    $store = fluxservice_tempstore("fluxservice.account.{$this->bundle()}");
    $store->delete($this->identifier());
  }

  /**
   * Helper function for loading Mailchimp user data into a linked account.
   *
   * @param array $response
   *   The response from the access token GET request.
   */
  protected function processAuthorizedAccount(array $response) {
    $settings = array_intersect_key($response, $this->getDefaultSettings());
    $this->data->mergeArray($settings);

    $this->setLabel($response['accountname'])
         ->setRemoteIdentifier($response['accountname']);
  }

  /**
   * {@inheritdoc}
   */
  public function accessOAuthCallback() {
    // Ensure that all required request and account values are set.
    if (!isset($_REQUEST['code'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Retrieves an access token from the Mailchimp API.
   *
   * @param string $code
   *   The access code returned from the Mailchimp API login step.
   * @param string $redirect
   *   The URL to redirect to. This has to match the redirect URL used in the
   *   login step.
   *
   * @return array
   *   An array containing the requested access token.
   */
  public function requestAccessToken($code, $redirect) {
    $service = $this->getService();

    // Retrieve a fresh request token from the Twitter API.
    $client = new Client($this->serviceUrl);

    // Add the request parameters.
    $request = $client->post($this->getAccessTokenUrl());
    $request->addPostFields(array(
      'client_id' => $service->getConsumerKey(),
      'client_secret' => $service->getConsumerSecret(),
      'redirect_uri' => $redirect,
      'code' => $code,
      'grant_type' => 'authorization_code',
    ));

    // Issue an access token request.
    $response = $request->send()->json();

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function requestExtendedAccessToken($token) {
    $service = $this->getService();
    // Extend a previously requested access token.
    $client = new Client($this->serviceUrl);

    // Add the request parameters.
    $auth_headers = array('Authorization' => "OAuth $token");
    $request = $client->get($this->getMetadataUrl(), $auth_headers);

    // Issue an access token request.
    $response = $request->send()->json();

    return $response;
  }

  protected function getRedirectUrl() {
    return url("fluxservice/oauth/{$this->bundle()}/{$this->identifier()}", array('absolute' => TRUE));
  }
  /**
   * {@inheritdoc}
   */
  protected function getAuthorizeUrl() {
    return "oauth2/authorize";
  }

  /**
   * {@inheritdoc}
   */
  protected function getAccessTokenUrl() {
    return "oauth2/token";
  }

  /**
   * {@inheritdoc}
   */
  protected function getMetadataUrl() {
    return "oauth2/metadata";
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken() {
    return $this->data->get('access_token');
  }

  /**
   * {@inheritdoc}
   */
  public function getDcString() {
    return $this->data->get('dc');
  }
}
