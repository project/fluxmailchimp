<?php

/**
 * @file
 * Contains MailchimpService.
 */

namespace Drupal\fluxmailchimp\Plugin\Service;

use Drupal\fluxservice\Service\OAuthServiceBase;
use Guzzle\Service\Builder\ServiceBuilder;

/**
 * Service plugin implementation for Mailchimp.
 */
class MailchimpService extends OAuthServiceBase implements MailchimpServiceInterface {

  /**
   * Defines the plugin.
   */
  public static function getInfo() {
    return array(
      'name' => 'fluxmailchimp',
      'label' => t('Mailchimp'),
      'description' => t('Provides Mailchimp integration for fluxkraft.'),
      'icon' => 'images/fluxicon_mailchimp.png',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return array(
      'service_url' => '',
      'client_id' => '',
      'client_secret' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array &$form_state) {

    $form['help'] = array(
      '#type' => 'markup',
      '#markup' => t('In the following, you need to provide authentication details for communicating with Mailchimp.'),
      '#prefix' => '<p class="fluxservice-help">',
      '#suffix' => '</p>',
      '#weight' => -1,
    );

    $form['client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Client identifier'),
      '#default_value' => $this->getConsumerKey(),
    );

    $form['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Client secret'),
      '#default_value' => $this->getConsumerSecret(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerKey() {
    return $this->data->get('client_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumerSecret() {
    return $this->data->get('client_secret');
  }
}
