<?php

/**
 * @file
 * Contains MailchimpAccountInterface
 */

namespace Drupal\fluxmailchimp\Plugin\Service;

use Drupal\fluxservice\Service\OAuthAccountInterface;

/**
 * Interface for Mailchimp accounts.
 */
interface MailchimpAccountInterface extends OAuthAccountInterface {

  /**
   * Gets the account's access token.
   *
   * @return string
   *   The access token of the account.
   */
  public function getAccessToken();

  /**
   * Gets the account's dc string.
   *
   * @return string
   *   The access token of the account.
   */
  public function getDcString();

  /**
   * Gets the Mailchimp client object.
   * The web service client for the Mailchimp API.
   *
   * @return \ZfrMailChimp\Client
   */
  public function client();
}
