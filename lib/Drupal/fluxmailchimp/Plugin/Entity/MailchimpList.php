<?php

/**
 * @file
 * Contains MailchimpList.
 */

namespace Drupal\fluxmailchimp\Plugin\Entity;

use Drupal\fluxservice\Entity\FluxEntityInterface;
use Drupal\fluxservice\Entity\RemoteEntity;

/**
 * Entity class for Mailchimp Contacts.
 */
class MailchimpList extends RemoteEntity implements MailchimpListInterface {

  /**
   * Defines the entity type.
   *
   * This gets exposed to hook_entity_info() via fluxservice_entity_info().
   */
  public static function getInfo() {
    return array(
      'name' => 'fluxmailchimp_list',
      'label' => t('Mailchimp: List'),
      'module' => 'fluxmailchimp',
      'service' => 'fluxmailchimp',
      'controller class' => '\Drupal\fluxmailchimp\MailchimpListController',
      'label callback' => 'entity_class_label',
      'entity keys' => array(
        'id' => 'drupal_entity_id',
        'remote id' => 'id',
      ),
    );
  }

  /**
   * Gets the entity property definitions.
   */
  public static function getEntityPropertyInfo($entity_type, $entity_info) {
    $info['id'] = array(
      'label' => t('Remote identifier'),
      'description' => t('The unique remote identifier of the Contact.'),
      'type' => 'text',
    );

    $info['web_id'] = array(
      'label' => t('Web Id'),
      'description' => t('The list id used in our web app, allows you to create a link directly to it.'),
      'type' => 'integer',
    );

    $info['name'] = array(
      'label' => t('Name'),
      'description' => t('The name of the list.'),
      'type' => 'text',
    );

    $info['date_created'] = array(
      'label' => t('Date Created'),
      'description' => t('The date that this list was created.'),
      'type' => 'date',
      'getter callback' => 'fluxservice_entity_property_getter_method',
    );

    $info['email_type_option'] = array(
      'label' => t('Email Type'),
      'description' => t('Whether or not the List supports multiple formats for emails or just HTML.'),
      'type' => 'boolean',
    );

    $info['default_from_name'] = array(
      'label' => t('Default From Name'),
      'description' => t('Default From Name for campaigns using this list'),
      'type' => 'text',
    );

    $info['default_from_email'] = array(
      'label' => t('Default From Email'),
      'description' => t('Default From Email for campaigns using this list'),
      'type' => 'text',
    );

    $info['default_subject'] = array(
      'label' => t('Default Subject'),
      'description' => t('Default Subject Line for campaigns using this list'),
      'type' => 'text',
    );

    $info['stats'] = array(
      'label' => t('Stats'),
      'description' => t('various stats and counts for the list - many of these are cached for at least 5 minutes.'),
      'type' => 'struct',
    );

    return $info;
  }

  /**
   * The name of the list.
   *
   * @var string
   */
  public $name;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getStats($id) {
    return $this->stats[$id];
  }



}

