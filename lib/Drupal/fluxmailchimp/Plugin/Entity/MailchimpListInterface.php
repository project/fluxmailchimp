<?php

/**
 * @file
 * Contains MailchimpListInterface.
 */

namespace Drupal\fluxmailchimp\Plugin\Entity;

use Drupal\fluxservice\Entity\RemoteEntityInterface;

/**
 * Provides a common interface for all Mailchimp objects.
 */
interface MailchimpListInterface extends RemoteEntityInterface {

}
