<?php

/**
 * @file
 * Contains MailchimpSubscribe.
 */

namespace Drupal\fluxmailchimp\Plugin\Rules\Action;

//use Drupal\fluxmailchimp\Plugin\Entity\MailchimpList;
//use Drupal\fluxmailchimp\Plugin\Entity\MailchimpListInterface;
use Drupal\fluxmailchimp\Plugin\Service\MailchimpAccountInterface;
use Drupal\fluxmailchimp\Rules\RulesPluginHandlerBase;

/**
 * "Add a user to list" action.
 */
class MailchimpUnsubscribe extends MailchimpListBase implements \RulesActionHandlerInterface {

  /**
   * Defines the action.
   */
  public static function getInfo() {
    return static::getInfoDefaults() + array(
      'name' => 'fluxmailchimp_unsubscribe',
      'label' => t('Unsubscribe email to the list'),
      'parameter' => array(
        'account' => static::getAccountParameterInfo(),
        'list' => array(
          'type' => 'text',
          'label' => t('List'),
          'options list' => array(get_called_class(), 'getListOptions'),
        ),
        'email' => array(
          'type' => 'text',
          'label' => t('Email to subscribe'),
        ),
        'goodbye' => array(
          'type' => 'boolean',
          'description' => t('Flag to control whether the unsubscribe message is sent to the subscriber.'),
          'label' => t('Send confirmation message.'),
        ),
      ),
      'group' => t('Mailchimp'),
    );
  }

  /**
   * Executes the action.
   */
  public function execute(MailchimpAccountInterface $account, $list, $email, $goodbye) {
    try {
      $param = array(
        'id' => $list,
        'email' => array(
          'email' => $email,
        ),
        'send_goodbye' => (bool) $goodbye,
        'send_notify' => true,
      );

      $request = $account->client()->unsubscribe($param);
    }
    catch (\ZfrMailChimp\Exception\Ls\NotSubscribedException $e) {

    }
    catch (\ZfrMailChimp\Exception\Email\NotExistsException $e) {

    }
    catch (\Guzzle\Http\Exception\BadResponseException $e) {

    }
    catch (\Guzzle\Http\Exception\ServerErrorResponseException $e) {
      //$req = $e->getRequest();
      //$resp =$e->getResponse();
      //dvm($resp->getHeader('X-MailChimp-API-Error-Code'));
    }
    catch (Exception $e) {
    }


  }

  /**
   * {@inheritdoc}
   */
  public function form_alter(&$form, $form_state, $options) {
    $account_selected = !empty($this->element->settings['account']);
    $list_selected = !empty($this->element->settings['list']);

    $form['reload_account'] = array(
      '#weight' => $form['submit']['#weight'] + 1,
      '#type' => 'submit',
      '#name' => 'reload_account',
      '#value' => !$account_selected ? t('Continue') : t('Reload form'),
      '#limit_validation_errors' => array(array('parameter', 'account'), array('parameter', 'list')),
      '#submit' => array('rules_form_submit_rebuild'),
      '#ajax' => rules_ui_form_default_ajax('fade'),
      '#attributes' => array('class' => array('rules-hide-js')),
    );

    $form['reload_list'] = array(
      '#weight' => $form['submit']['#weight'] + 2,
      '#type' => 'submit',
      '#name' => 'reload_list',
      '#value' => !$list_selected ? t('Continue') : t('Reload form'),
      '#limit_validation_errors' => array(array('parameter', 'list')),
      '#submit' => array('rules_form_submit_rebuild'),
      '#ajax' => rules_ui_form_default_ajax('fade'),
      '#attributes' => array('class' => array('rules-hide-js')),
    );

    // Use ajax and trigger as the reload button.
    $form['parameter']['account']['settings']['account']['#ajax'] = $form['reload_account']['#ajax'] + array(
      'event' => 'change',
      'trigger_as' => array('name' => 'reload_account'),
    );

    $form['parameter']['list']['settings']['list']['#ajax'] = $form['reload_list']['#ajax'] + array(
      'event' => 'change',
      'trigger_as' => array('name' => 'reload_list'),
    );

    if (empty($account_selected)) {
      unset($form['parameter']['list']);
      unset($form['parameter']['email']);
      unset($form['parameter']['goodbye']);
      $form['reload_account']['#limit_validation_errors'] = array(array('parameter', 'account'));
    }

    if (empty($list_selected)) {
      unset($form['parameter']['email']);
      unset($form['parameter']['goodbye']);
      $form['reload_list']['#limit_validation_errors'] = array(array('parameter', 'list'));
    }
  }

}
