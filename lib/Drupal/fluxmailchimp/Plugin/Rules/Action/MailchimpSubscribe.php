<?php

/**
 * @file
 * Contains MailchimpSubscribe.
 */

namespace Drupal\fluxmailchimp\Plugin\Rules\Action;

//use Drupal\fluxmailchimp\Plugin\Entity\MailchimpList;
//use Drupal\fluxmailchimp\Plugin\Entity\MailchimpListInterface;
use Drupal\fluxmailchimp\Plugin\Service\MailchimpAccountInterface;
use Drupal\fluxmailchimp\Rules\RulesPluginHandlerBase;

/**
 * "Add a user to list" action.
 */
class MailchimpSubscribe extends MailchimpListBase implements \RulesActionHandlerInterface {

  /**
   * Defines the action.
   */
  public static function getInfo() {
    return static::getInfoDefaults() + array(
      'name' => 'fluxmailchimp_subscribe',
      'label' => t('Subscribe email to the list'),
      'parameter' => array(
        'account' => static::getAccountParameterInfo(),
        'list' => array(
          'type' => 'text',
          'label' => t('List'),
          'options list' => array(get_called_class(), 'getListOptions'),
        ),
        'email' => array(
          'type' => 'text',
          'label' => t('Email to subscribe'),
        ),
        'fname' => array(
          'type' => 'text',
          'label' => t('Firstname'),
        ),
        'lname' => array(
          'type' => 'text',
          'label' => t('Lastname'),
        ),
        'double_optin' => array(
          'type' => 'boolean',
          'description' => t('Flag to control whether a double opt-in confirmation message is sent, defaults to true. <em>Abusing this may cause your account to be suspended.</em>'),
          'label' => t('Send confirmation message.'),
        ),
        'profile' => array(
          'type' => 'profile2',
          'label' => t('Profile (for interest groups)'),
          'required' => FALSE
        ),
        'interests' => array(
          'type' => 'text',
          'label' => t('Interest Groups'),
          'required' => FALSE
        ),
        'merge_vars' => array(
          'type' => 'text',
          'label' => t('Merge Variables'),
          'required' => FALSE
        ),
      ),
      'group' => t('Mailchimp'),
    );
  }

  /**
   * Executes the action.
   */
  public function execute(MailchimpAccountInterface $account, $list, $email, $firstname, $lastname, $double_optin, $profile, $interests, $merge_vars = array()) {
    if (empty($email) || !valid_email_address($email)) {
      return;
    }

    try {
      $param = array(
        'id' => $list,
        'email' => array(
          'email' => $email,
        ),
        'merge_vars' => $merge_vars,
        'update_existing' => true,
        'double_optin' => (bool) $double_optin,
      );

      // @todo - replace these with merge_vars
      if (!empty($firstname)) $param['merge_vars']['FNAME'] = $firstname;
      if (!empty($lastname))  $param['merge_vars']['LNAME'] = $lastname;

      // Make sure interest groups setup.
      self::updateInterestGroups($account, $list, $interests);

      foreach ($interests as $interest) {
        $items = field_get_items('profile2', $profile, $interest);
        if (is_array($items) && count($items) > 0) {
          $groups = array();
          foreach($items as $item) {
            if (!empty($item['value'])) {
              $groups[] = $item['value'];
            }
          }
          if (!empty($groups)) {
            $param['merge_vars']['groupings'][] = array('name' => $interest, 'groups' => $groups);
          }
        }
      }

      // Make sure interest groups setup.
      self::updateMergeVars($account, $list, $merge_vars);
      foreach ($merge_vars as $var => $tag) {
        $items = field_get_items('profile2', $profile, $var);
        if (is_array($items) && count($items) > 0) {
          $value = NULL;
          foreach($items as $item) {
            if (!empty($item['value'])) {
              $value = $item['value'];
            }
            else {
              $value = filter_xss(drupal_render(field_view_field('profile2', $profile, $var, array('label' => 'hidden'))), array());
            }
          }
          if (!empty($value)) {
            $param['merge_vars'][$tag] = $value;
          }
        }
      }

      $request = $account->client()->subscribe($param);
    }
    catch (\ZfrMailChimp\Exception\Email\NotExistsException $e) {
      // not an issue - if its not there anyway
    }
    catch (\Guzzle\Http\Exception\BadResponseException $e) {
    }
    catch (\Guzzle\Http\Exception\ServerErrorResponseException $e) {
      //$req = $e->getRequest();
      //$resp =$e->getResponse();
      //dvm($resp->getHeader('X-MailChimp-API-Error-Code'));
    }
    catch (Exception $e) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form_alter(&$form, $form_state, $options) {
    $account_selected = !empty($this->element->settings['account']);
    $list_selected = !empty($this->element->settings['list']);

    $form['reload_account'] = array(
      '#weight' => $form['submit']['#weight'] + 1,
      '#type' => 'submit',
      '#name' => 'reload_account',
      '#value' => !$account_selected ? t('Continue') : t('Reload form'),
      '#limit_validation_errors' => array(array('parameter', 'account'), array('parameter', 'list')),
      '#submit' => array('rules_form_submit_rebuild'),
      '#ajax' => rules_ui_form_default_ajax('fade'),
      '#attributes' => array('class' => array('rules-hide-js')),
    );

    $form['reload_list'] = array(
      '#weight' => $form['submit']['#weight'] + 2,
      '#type' => 'submit',
      '#name' => 'reload_list',
      '#value' => !$list_selected ? t('Continue') : t('Reload form'),
      '#limit_validation_errors' => array(array('parameter', 'list')),
      '#submit' => array('rules_form_submit_rebuild'),
      '#ajax' => rules_ui_form_default_ajax('fade'),
      '#attributes' => array('class' => array('rules-hide-js')),
    );

    $form['parameter']['interests']['settings']['interests']['#type'] = 'checkboxes';
    $form['parameter']['interests']['settings']['interests']['#options'] = self::getFieldOptions('personal');
    //$form['parameter']['interests']['settings'] = array();
    //$form['parameter']['interests']['#description'] = print_r($form, TRUE);

    // Use ajax and trigger as the reload button.
    $form['parameter']['account']['settings']['account']['#ajax'] = $form['reload_account']['#ajax'] + array(
      'event' => 'change',
      'trigger_as' => array('name' => 'reload_account'),
    );

    $form['parameter']['list']['settings']['list']['#ajax'] = $form['reload_list']['#ajax'] + array(
      'event' => 'change',
      'trigger_as' => array('name' => 'reload_list'),
    );

    if (empty($account_selected)) {
      unset($form['parameter']['list']);
      unset($form['parameter']['email']);
      unset($form['parameter']['fname']);
      unset($form['parameter']['lname']);
      unset($form['parameter']['double_optin']);
      unset($form['parameter']['profile']);
      unset($form['parameter']['interests']);
      unset($form['parameter']['merge_vars']);
      $form['reload_account']['#limit_validation_errors'] = array(array('parameter', 'account'));
    }

    if (empty($list_selected)) {
      unset($form['parameter']['email']);
      unset($form['parameter']['fname']);
      unset($form['parameter']['lname']);
      unset($form['parameter']['double_optin']);
      unset($form['parameter']['profile']);
      unset($form['parameter']['interests']);
      unset($form['parameter']['merge_vars']);
      $form['reload_list']['#limit_validation_errors'] = array(array('parameter', 'list'));
    }
  }


}
