<?php

/**
 * @file
 * Contains RulesPluginHandlerBase.
 */

namespace Drupal\fluxmailchimp\Rules;

use Drupal\fluxservice\Rules\FluxRulesPluginHandlerBase;

/**
 * Base class for mailchimp Rules plugin handler.
 */
abstract class RulesPluginHandlerBase extends FluxRulesPluginHandlerBase {

  /**
   * Returns info-defaults for mailchimp plugin handlers.
   */
  public static function getInfoDefaults() {
    return array(
      'category' => 'fluxmailchimp',
      'access callback' => array(get_called_class(), 'integrationAccess'),
    );
  }

  /**
   * Rules mailchimp integration access callback.
   */
  public static function integrationAccess($type, $name) {
    return fluxservice_access_by_plugin('fluxmailchimp');
  }

  /**
   * Returns info suiting for mailchimp service account parameters.
   */
  public static function getAccountParameterInfo() {
    return array(
      'type' => 'fluxservice_account',
      'bundle' => 'fluxmailchimp',
      'label' => t('Mailchimp account'),
      'description' => t('The Mailchimp account under which this shall be executed.'),
    );
  }
}
